# ucfirst
- [ ] 

Uppercase first letter of the first word in a string, or uppercase the first letter on the the first word for each entry in an array.
Example:
var str = 'this is the test string';
str.ucFirst();
Result: This is the test stringResult

str.split(' ').ucFirst().join(' ');
Result: This Is The Test String