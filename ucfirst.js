if (!String.prototype.ucFirst) {
  Object.defineProperty(String.prototype, 'ucFirst', {
	value: function () {
	  return this.slice(0, 1).toUpperCase() + this.slice(1).toLowerCase();
	},
	enumerable: false
  });
}
if (!Array.prototype.ucFirst) {
  Object.defineProperty(Array.prototype, 'ucFirst', {
	value: function () {
	  var arr = this, i, j;
	  for(i = 0, j = arr.length; i < j; i++) {
		arr[i] = arr[i].slice(0, 1).toUpperCase() + arr[i].slice(1).toLowerCase();
	  }
	  return arr;
	},
	enumerable: false
  });
}
